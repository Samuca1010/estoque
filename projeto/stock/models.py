from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User

# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=45)
    description = models.TextField()
    quantidade = models.PositiveIntegerField()
    image = models.ImageField(null=True, upload_to='product')
    codigo = models.CharField(max_length=20)

    def __str__(self):
        return self.title 